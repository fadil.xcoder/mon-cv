# Notes

**CV based on JSON Resume**

- `symfony new 05-mon-cv --webapp`
- Install `composer require symfonycasts/tailwind-bundle`
- Run to initialize `symfony console tailwind:init`
- Build css `symfony console tailwind:build`
- Build css in watch mode `symfony console tailwind:build --watch`
- Install `symfony console importmap:require alpinejs`
- Asset Mapper
- - `symfony console asset-map:compile`
- - `symfony console debug:asset-map`

https://drive.google.com/file/d/1X0TBhPMMipPQ7zHkcMWVmtaKCjObv0Zn/view?usp=sharing

**Tools**

- https://symfony.com/doc/current/frontend/asset_mapper.html
- https://www.hyperui.dev/components/marketing/headers
- https://alpinejs.dev/
- https://hyperjs-pvrcchd9l-markmead.vercel.app/examples/alert-close
- https://alpinejs.dev/essentials/installation
- https://tailwindcss.com/docs/installation/play-cdn

**URLS**

- - http://127.0.0.1:8000/hyper-ui (Testing HyperUI library - Tailwind + Alpinejs)
